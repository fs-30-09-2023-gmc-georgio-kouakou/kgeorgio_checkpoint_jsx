import DrogbaREACT from "../img/DrogbaREACT.png"
import lewandowskiREACT from "../img/lewandowskiREACT.png"
import MessiREACT from "../img/messiREACT.png"
import RonaldoREACT from "../img/RonaldoREACT.png"

export const players=[
    {
        "Name" : "Messi",
        "PAC" : 87,
        "SHO" : 92,
        "PAS" : 92,
        "DRI" : 96,
        "DEF" : 39,
        "PHY" : 66,
        "TEAM" : "Barcelone",
        "NATIONALITY": "Argentin",
        "JERSEY_NUM" : 10,
        "AGE": 36,
        "ImageURL" : MessiREACT
    },

    {
        "Name" : "Cristiano Ronaldo",
        "PAC" : 85,
        "SHO" : 90,
        "PAS" : 77,
        "DRI" : 84,
        "DEF" : 35,
        "PHY" : 75,
        "TEAM" : "Real Madrid",
        "NATIONALITY": "Portugese",
        "JERSEY_NUM" : 7,
        "AGE": 37,
        "ImageURL" : RonaldoREACT
    },

    {
        "Name" : "Lewandowski",
        "PAC" : 75,
        "SHO" : 91,
        "PAS" : 80,
        "DRI" : 87,
        "DEF" : 44,
        "PHY" : 84,
        "TEAM" : "Bayern",
        "NATIONALITY": "Polonese",
        "JERSEY_NUM" : 9,
        "AGE": 34,
        "ImageURL" : lewandowskiREACT
    },

    {
        "Name" : "Drogba",
        "PAC" : 87,
        "SHO" : 90,
        "PAS" : 74,
        "DRI" : 80,
        "DEF" : 44,
        "PHY" : 87,
        "TEAM" : "Chelsea",
        "NATIONALITY": "Ivoirian",
        "JERSEY_NUM" : 11,
        "AGE": 30,
        "ImageURL" : DrogbaREACT
    }
]