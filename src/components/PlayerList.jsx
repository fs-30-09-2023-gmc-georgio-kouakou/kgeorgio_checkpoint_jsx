import { players } from "../custom/players"
import { PlayerCard } from "./Player"

export const PlayerList = (props) =>{
    return(
        players.map((single_player)=>{
            return(
                <PlayerCard image={single_player.ImageURL} name={single_player.Name} team={single_player.TEAM} nationality={single_player.NATIONALITY} jersey_num={single_player.JERSEY_NUM} age={single_player.AGE}  />
            )
        })
    )
}