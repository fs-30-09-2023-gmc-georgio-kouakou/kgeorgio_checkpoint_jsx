// import { players } from "../custom/players"
import { Card } from "react-bootstrap"

// export const PlayerCard = ({image, name, team, nationality, jersey_num, age}) =>{
//     return(
//         players.map((single_player) => {
//             return(
//                 <Card style={{ width: '18rem' }}>
//                 <Card.Img variant="top" src={single_player.ImageURL} />
//                 <Card.Body>
//                     <Card.Title>{single_player.Name}</Card.Title>
//                     <Card.Text>
//                             <p>TEAM : {single_player.TEAM}</p>
//                             <p>NATIONALITY : {single_player.NATIONALITY}</p>
//                             <p>JERSEY_NUMBER : {single_player.JERSEY_NUM}</p>
//                             <p>AGE : {single_player.AGE}</p>
//                     </Card.Text>
//                 </Card.Body>
//             </Card>
//             )

//         })
//     )
// }
export const PlayerCard = ({ image, name, team, nationality, jersey_num, age }) => {
    return (
        <Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src={image} />
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>
                    <p>TEAM : {team}</p>
                    <p>NATIONALITY : {nationality}</p>
                    <p>JERSEY_NUMBER : {jersey_num}</p>
                    <p>AGE : {age}</p>
                </Card.Text>
            </Card.Body>
        </Card>
    )
}


